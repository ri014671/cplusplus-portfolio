#include <iostream>
#include <string>
#include <vector>
#include <unordered_map>
#include <memory>
#include <algorithm>
#include <fstream>
#include <sstream>

// Forward declaration
class Room;

// Abstract GameObject
class GameObject {
public:
    virtual void interact() = 0;
    virtual ~GameObject() = default;
};

// Item class
class Item : public GameObject {
private:
    std::string name;
    std::string description;

public:
    Item(std::string name, std::string description) : name(std::move(name)), description(std::move(description)) {}

    void interact() override {
        std::cout << "You look at the " << name << ": " << description << std::endl;
    }

    std::string getName() const {
        return name;
    }
};

// NPC class
class NPC : public GameObject {
private:
    std::string name;
    std::string dialogue;

public:
    NPC(std::string name, std::string dialogue) : name(std::move(name)), dialogue(std::move(dialogue)) {}

    void interact() override {
        std::cout << name << " says: " << dialogue << std::endl;
    }

    std::string getName() const {
        return name;
    }
};

// Room class
class Room {
private:
    std::string description;
    std::unordered_map<std::string, Room*> exits;
    std::vector<std::unique_ptr<GameObject>> objects;
    bool isEndingRoom = false;
    std::string endingType;

public:
    Room(std::string description, bool isEnding = false, std::string endingType = "") : description(std::move(description)), isEndingRoom(isEnding), endingType(std::move(endingType)) {}

    void addExit(const std::string& direction, Room* room) {
        exits[direction] = room;
    }

    Room* getExit(const std::string& direction) const {
        auto it = exits.find(direction);
        if (it != exits.end()) {
            return it->second;
        }
        return nullptr;
    }

    void addObject(std::unique_ptr<GameObject> object) {
        objects.push_back(std::move(object));
    }

    void describe() const {
        std::cout << description << std::endl;
        if (isEndingRoom) {
            std::cout << "This is the end of your journey. " << endingType << std::endl;
        }
        else {
            for (const auto& object : objects) {
                const Item* item = dynamic_cast<const Item*>(object.get());
                const NPC* npc = dynamic_cast<const NPC*>(object.get());
                if (item) {
                    std::cout << "There is a " << item->getName() << " here." << std::endl;
                }
                else if (npc) {
                    std::cout << "You see " << npc->getName() << " here." << std::endl;
                }
            }
        }
    }

    void interactWith(const std::string& name) {
        for (const auto& object : objects) {
            const Item* item = dynamic_cast<const Item*>(object.get());
            const NPC* npc = dynamic_cast<const NPC*>(object.get());
            if ((item && item->getName() == name) || (npc && npc->getName() == name)) {
                object->interact();
                return;
            }
        }
        std::cout << "There is nothing named " << name << " to interact with here." << std::endl;
    }

    bool getIsEndingRoom() const {
        return isEndingRoom;
    }

    std::string getEndingType() const {
        return endingType;
    }
};

// Player class
class Player {
private:
    std::string name;
    Room* currentRoom;

public:
    Player(std::string name, Room* startRoom) : name(std::move(name)), currentRoom(startRoom) {}

    void moveTo(const std::string& direction) {
        Room* nextRoom = currentRoom->getExit(direction);
        if (nextRoom) {
            currentRoom = nextRoom;
            std::cout << "You move to the " << direction << " exit." << std::endl;
            currentRoom->describe();
            if (currentRoom->getIsEndingRoom()) {
                std::cout << "Game Over: " << currentRoom->getEndingType() << std::endl;
                exit(0); // Optionally, handle game ending more gracefully
            }
        }
        else {
            std::cout << "There's no path leading " << direction << " from here." << std::endl;
        }
    }

    void interactWith(const std::string& name) {
        currentRoom->interactWith(name);
    }
};

// GameManager class
class GameManager {
private:
    std::unordered_map<std::string, std::unique_ptr<Room>> rooms;
    std::unique_ptr<Player> player;

public:
    void addRoom(const std::string& name, std::unique_ptr<Room> room) {
        rooms[name] = std::move(room);
    }

    Room* getRoom(const std::string& name) {
        auto it = rooms.find(name);
        return it != rooms.end() ? it->second.get() : nullptr;
    }

    void connectRooms(const std::string& room1Name, const std::string& room2Name, const std::string& direction) {
        Room* room1 = getRoom(room1Name);
        Room* room2 = getRoom(room2Name);
        if (room1 && room2) {
            room1->addExit(direction, room2);
        }
    }

    void loadMapFromFile(const std::string& game_map) {
        std::ifstream file(game_map);
        if (file.is_open()) {
            std::string line;
            while (std::getline(file, line)) {
                std::istringstream iss(line);
                std::string roomName, description, isEnding, endingType;
                std::getline(iss, roomName, '|');
                std::getline(iss, description, '|');
                std::getline(iss, isEnding, '|');
                std::getline(iss, endingType, '|');

                bool isEndingRoom = isEnding == "true";
                auto room = std::make_unique<Room>(description, isEndingRoom, endingType);
                addRoom(roomName, std::move(room));
            }
            file.close();
        }
        else {
            std::cerr << "Unable to open file: " << game_map << std::endl;
        }
    }

    void setupGame(const std::string& playerName, const std::string& startRoomName) {
        player = std::make_unique<Player>(playerName, getRoom(startRoomName));
    }

    void play() {
        // Placeholder for the main game loop
        // Here you could handle player commands and game logic
    }
};

int main() {
    GameManager gameManager;
    gameManager.loadMapFromFile("game_map.txt"); // Ensure this file exists with proper format

    // Setup rooms and connections manually or via file
    // Example of manual setup
    // auto room1 = std::make_unique<Room>("Room 1 Description");
    // auto room2 = std::make_unique<Room>("Room 2 Description", true, "Victory!");
    // gameManager.addRoom("Room1", std::move(room1));
    // gameManager.addRoom("Room2", std::move(room2));
    // gameManager.connectRooms("Room1", "Room2", "east");

    gameManager.setupGame("PlayerName", "StartRoomName");
    gameManager.play();
}
